const mongoose = require('mongoose');

let productSchema = mongoose.Schema({
    page_description:    {type: 'string', required: "true"},
    page_title:          {type: 'string', required: "true"},
    name:                {type: 'string', required: "true"},
    c_isNewtest:         {type: 'boolean', required: "true"},
    price:               {type: 'number', required: "true"},
    currency:            {type: 'string', required: "true"},
    primary_category_id: {type: 'string', required: "true"},
    orderable:           {type: 'boolean', required: "true"},
    image_groups: [{
            images:
            [{
                alt:     {type: 'string', required: "true"},
                link:    {type: 'string', required: "true"},
                title:   {type: 'string', required: "true"},
            }]
    }],
    long_description: {type: 'string', required: "true"},
});

let Product = mongoose.model('Product', productSchema);

module.exports = Product;