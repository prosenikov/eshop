const mongoose = require('mongoose');

let categorySchema = mongoose.Schema({
    name:                  {type: 'string', required: "true"},
    page_description:      {type: 'string', required: "true"},
    page_title:            {type: 'string', required: "true"},
    parent_category_id:    {type: 'string', required: "true"},
    image:                 {type: 'string', required: "true"},
    numIndex:              {type: 'number', required: "false"},
    categories: [{
        id:                 {type: 'string', required: "true"},
        name:               {type: 'string', required: "true"},
        page_description:   {type: 'string', required: "true"},
        page_title:         {type: 'string', required: "true"},
        parent_category_id: {type: 'string', required: "true"},
        image:              {type: 'string', required: "true"},
        categories: [{
            id:                 {type: 'string', required: "true"},
            image:              {type: 'string', required: "true"},
            name:               {type: 'string', required: "true"},
            page_description:   {type: 'string', required: "true"},
            page_title:         {type: 'string', required: "true"},
            parent_category_id: {type: 'string', required: "true"},
        }]
    }]
    });

let Category = mongoose.model('Category', categorySchema);

module.exports = Category;