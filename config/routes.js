const productController = require('../controllers/product');

module.exports = (app) => {
    app.get('/', productController.index);
    app.get('/categories/:name', productController.getCategory);
    app.get('/product/:id', productController.productGet);
    app.get('/buy/:id', productController.productBuy);
};