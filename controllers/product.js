const Product = require('../models/Product');
const Category = require('../models/Category');
let request = require('request');

module.exports = {
	index: (req, res) => {
		Category.find().limit(20).then(categories => {
          return res.render("product/categories", {categories});
		}).catch(err =>{
            return res.send("Error");
		});
	},
    getCategory: (req, res) => {
	    let name = req.params.name;

        let searchTerm = "";
        if (name === "Mens")
            searchTerm = "mens";
        else if (name === "Womens")
            searchTerm = "womens";
        Category.find({'name': searchTerm}).then(category => {
            let query = "";
            if (searchTerm === "")
            {
                query = Product.find({
                    'primary_category_id': name
                });
            }
            else {
                let regExp = new RegExp("^" + searchTerm + ".*");
                query = Product.find({
                    'primary_category_id': regExp,
                });
            }
            query.exec(function (err, products) {
                if (err) {
                    console.log(err);
                    return res.redirect("/");
                }
                return res.render("product/get", {category, name, products});
            });
        }).catch(err =>{
            console.log(err);
            return res.redirect(":/");
        });
    },
    productGet: (req, res) => {

	    let id = req.params.id;
        // Make an REQUEST to EUR/USD Exchange API
        let URL = "https://api.fixer.io/latest?base=USD&symbols=EUR";
        request(URL, function(err, request){
            let json = request.body;
            let data = JSON.parse(json);
            let usdToEUR = Number.parseFloat(data.rates["EUR"]);

	    Product.findById(id).then(product =>
        {
            return res.render("product/info", {product, usdToEUR});
        }).catch(err => { res.redirect(":/"); });
        });
    },
    productBuy: (req, res) => {

        let id = req.params.id;

        Product.findById(id).then(product =>
        {
            return res.render("product/buy", {product});
        }).catch(err => { res.redirect(":/"); });
    },
};