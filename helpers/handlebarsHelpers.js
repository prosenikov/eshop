const Handlebars = require('hbs');
Handlebars.registerHelper("checkif", require('handlebars-helper-checkif'));
Handlebars.registerHelper("debug", function(optionalValue) {
    console.log("Current Context");
    console.log("====================");
    console.log(this);

    if (optionalValue) {
        console.log("Value");
        console.log("====================");
        console.log(optionalValue);
    }
});
Handlebars.registerHelper('ifCond', function(v1) {
    if(v1 !== "root") {
        console.log("Not root cat:", v1);
        return true;
    }
    console.log("Root cat:", v1);
    return false;
});
Handlebars.registerHelper('timesCat', function(block) {
    let imagesArr = block[0].images;
    let link = "<img src=\"/images/"+ imagesArr[0].link + "\" class=\"catBigImage\" />";
      // console.log(link);
    return link;
});
Handlebars.registerHelper('bigImage', function(block) {
    let imagesArr = block[0].images;
    let link = "<img src=\"/images/"+ imagesArr[0].link + "\" class=\"prodBigImage\" />";
    // console.log(link);
    return link;
});